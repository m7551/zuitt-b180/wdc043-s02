import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        String[] fruitArray = new String[5];
        fruitArray[0] = "Apple";
        fruitArray[1] = "Avocado";
        fruitArray[2] = "Banana";
        fruitArray[3] = "Kiwi";
        fruitArray[4] = "Orange";

        System.out.println(Arrays.toString(fruitArray));
        System.out.println("Which fruit would you like to get the index of?");

        String fruit = userInput.nextLine();
        int fruitIndex = Arrays.binarySearch(fruitArray, fruit);
        System.out.println("The index of " + fruit + " is: " + fruitIndex);

        ArrayList<String> friends = new ArrayList<>();
        friends.add("Connie");
        friends.add("Jean");
        friends.add("Sasha");
        friends.add("Levi");
        System.out.println("My friends are: " + friends);

        Map<String, Integer> inventory = new HashMap<String, Integer>();
        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);






    }
}